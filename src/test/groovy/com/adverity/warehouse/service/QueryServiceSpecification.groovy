package com.adverity.warehouse.service

import com.adverity.warehouse.helper.WarehouseSpecificationHelper
import com.adverity.warehouse.model.type.Aggregate
import com.adverity.warehouse.model.type.Dimension
import com.adverity.warehouse.service.dto.QueryResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

@Transactional
@SpringBootTest
class QueryServiceSpecification extends Specification {

    @Subject
    @Autowired
    QueryService queryService

    @Unroll
    def "Should successfully query for warehouse data with aggregates: #aggregates"() {
        when: "querying for warehouse data with aggregates"
        List<QueryResult> queryResults = queryService.query(aggregates, [] as Set, [] as Set, 10, 1)

        then: "we should get expected results"
        queryResults.sort() == result.sort()

        where:
        aggregates                                     || result
        [Aggregate.CLICK] as Set                       || [new QueryResult(clicks: 148)]
        [Aggregate.IMPRESSION] as Set                  || [new QueryResult(impressions: 1396)]
        [Aggregate.CLICK, Aggregate.IMPRESSION] as Set || [new QueryResult(clicks: 148, impressions: 1396)]
    }

    @Unroll
    def "Should successfully query for warehouse data with group by: #groupBy"() {
        when: "querying for warehouse data with group by"
        List<QueryResult> queryResults = queryService.query([] as Set, groupBy, [] as Set, 10, 1)

        then: "we should get expected results"
        queryResults.sort() == result.sort()

        where:
        groupBy                                           || result
        [Dimension.DATASOURCE] as Set                     || [new QueryResult(datasource: "Test datasource 1"), new QueryResult(datasource: "Test datasource 2")]
        [Dimension.CAMPAIGN] as Set                       || [new QueryResult(campaign: "Test campaign 1"), new QueryResult(campaign: "Test campaign 2")]
        [Dimension.DATE] as Set                           || [new QueryResult(date: WarehouseSpecificationHelper.parseLocalDate("01/01/21")), new QueryResult(date: WarehouseSpecificationHelper.parseLocalDate("02/01/21")), new QueryResult(date: WarehouseSpecificationHelper.parseLocalDate("03/01/21"))]
        [Dimension.DATASOURCE, Dimension.CAMPAIGN] as Set || [new QueryResult(datasource: "Test datasource 1", campaign: "Test campaign 1"), new QueryResult(datasource: "Test datasource 1", campaign: "Test campaign 2"), new QueryResult(datasource: "Test datasource 2", campaign: "Test campaign 1"), new QueryResult(datasource: "Test datasource 2", campaign: "Test campaign 2")]
    }

    @Unroll
    def "Should successfully query for warehouse data with filters: #filters"() {
        when: "querying for warehouse data with filters"
        List<QueryResult> queryResults = queryService.query([Aggregate.CLICK] as Set, [] as Set, filters, 10, 1)

        then: "we should get expected results"
        queryResults.sort() == result.sort()

        where:
        filters                                                                                                                                                                                                                || result
        [[property: Dimension.DATASOURCE, values: ["Test datasource 1"]]] as Set                                                                                                                                               || [new QueryResult(clicks: 27)]
        [[property: Dimension.CAMPAIGN, values: ["Test campaign 1"]]] as Set                                                                                                                                                   || [new QueryResult(clicks: 122)]
        [[property: Dimension.DATE, values: [dateFrom: "01/01/21", dateTo: "02/01/21"]]] as Set                                                                                                                                || [new QueryResult(clicks: 127)]
        [[property: Dimension.DATASOURCE, values: ["Test datasource 1", "Test datasource 2"]], [property: Dimension.CAMPAIGN, values: ["Test campaign 1"]], [property: Dimension.DATE, values: [dateFrom: "03/01/21"]]] as Set || [new QueryResult(clicks: 1)]
    }

    @Unroll
    def "Should successfully query for warehouse data with page size: #pageSize and page number: #pageNumber"() {
        when: "querying for warehouse data with paging"
        List<QueryResult> queryResults = queryService.query([] as Set, [Dimension.DATASOURCE, Dimension.CAMPAIGN, Dimension.DATE] as Set, [] as Set, pageSize, pageNumber)

        then: "we should get expected results"
        queryResults.size() == numberOfResults

        where:
        pageSize | pageNumber || numberOfResults
        1        | 1           | 1
        2        | 2           | 2
        10       | 2           | 0
    }


    def "Should successfully query for total clicks of a datasource for a given date range"() {
        given: "a valid request for total clicks of a datasource for a given date range"
        Set aggregates = [Aggregate.CLICK]
        Set groupBy = [Dimension.DATASOURCE]
        Set filter = [
                [property: Dimension.DATASOURCE, values: ["Test datasource 1"]],
                [property: Dimension.DATE, values: [dateFrom: "01/01/21", dateTo: "02/01/21"]]
        ]

        when: "querying for warehouse data"
        List<QueryResult> queryResults = queryService.query(aggregates, groupBy, filter, 10, 1)

        then: "we should get expected results"
        queryResults == [new QueryResult(datasource: "Test datasource 1", clicks: 26)]
    }

    def "Should successfully query for click-through rate per datasource and campaign"() {
        given: "a valid request for click-through rate per datasource and campaign"
        Set aggregates = [Aggregate.CLICK_THROUGH_RATE]
        Set groupBy = [Dimension.DATASOURCE, Dimension.CAMPAIGN]

        when: "querying for warehouse data"
        List<QueryResult> queryResults = queryService.query(aggregates, groupBy, [] as Set, 10, 1)

        then: "we should get expected results"
        queryResults.size() == 4
        queryResults.clickThroughRate.sort() == [0.06774, 0.24000, 0.10000, 0.39216].sort()
    }

    def "Should successfully query for impressions over time"() {
        given: "a valid request for impressions over time"
        Set aggregates = [Aggregate.IMPRESSION]
        Set groupBy = [Dimension.DATE]

        when: "querying for warehouse data"
        List<QueryResult> queryResults = queryService.query(aggregates, groupBy, [] as Set, 10, 1)

        then: "we should get expected results"
        queryResults.size() == 3
        queryResults.impressions.sort() == [1111L, 225L, 60L].sort()
    }
}
