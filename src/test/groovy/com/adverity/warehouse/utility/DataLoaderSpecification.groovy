package com.adverity.warehouse.utility

import com.adverity.warehouse.model.Metric
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Transactional
@SpringBootTest(properties = ['com.adverity.warehouse.loadInitialData=false'])
class DataLoaderSpecification extends Specification {

    @Subject
    @Autowired
    DataLoader dataLoader

    def "Should successfully load data to database"() {
        when: "calling initial data load"
        dataLoader.loadData("test-data-loader.csv")

        then: "expected data should be stored in the database"
        List<Metric> metrics = Metric.findAll()

        metrics.size() == 2

        metrics.datasource
        metrics.campaign

        LocalDate expectedDate = LocalDate.parse("01/01/21", DateTimeFormatter.ofPattern(WarehouseConstants.DEFAULT_DATE_FORMAT))
        metrics.date == [expectedDate, expectedDate]

        metrics.clicks == [1L, 1L]
        metrics.impressions == [1L, 1L]
    }
}
