package com.adverity.warehouse.helper

import com.adverity.warehouse.utility.WarehouseConstants

import java.time.LocalDate

class WarehouseSpecificationHelper {
    static LocalDate parseLocalDate(String date) {
        LocalDate.parse(date, java.time.format.DateTimeFormatter.ofPattern(WarehouseConstants.DEFAULT_DATE_FORMAT))
    }
}
