package com.adverity.warehouse.controller

import com.adverity.warehouse.service.QueryService
import com.adverity.warehouse.service.dto.QueryResult
import com.adverity.warehouse.request.QueryRequest
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class WarehouseController {

    private QueryService queryService

    WarehouseController(QueryService queryService) {
        this.queryService = queryService
    }

    @Operation(summary = "Query warehouse data")
    @PostMapping("query")
    Map query(@RequestBody QueryRequest queryRequest) {
        if (!queryRequest.validate()) {
            return [success: false, validationError: "At least an aggregate or an group by property must be given"]
        }
        List<QueryResult> queryResults = queryService.query(
            queryRequest.aggregates, queryRequest.groupBy, queryRequest.filters, queryRequest.pageSize, queryRequest.pageNumber
        )

        [success: true, data: queryResults]
    }

}
