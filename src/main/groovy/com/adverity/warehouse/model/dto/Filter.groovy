package com.adverity.warehouse.model.dto

import com.adverity.warehouse.model.type.Dimension

class Filter {
    Dimension property
    def values
}
