package com.adverity.warehouse.model.type

enum Aggregate {
    CLICK, IMPRESSION, CLICK_THROUGH_RATE
}
