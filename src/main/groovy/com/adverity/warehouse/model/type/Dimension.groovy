package com.adverity.warehouse.model.type

enum Dimension {
    DATASOURCE, CAMPAIGN, DATE
}