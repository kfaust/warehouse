package com.adverity.warehouse.model

abstract class MetricAggregate {
    String name

    static hasMany = [metrics: Metric]

    static mapping = {
        version false
    }
}
