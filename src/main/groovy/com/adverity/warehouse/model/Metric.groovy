package com.adverity.warehouse.model

import grails.gorm.annotation.Entity

import java.time.LocalDate

@Entity
class Metric {
    LocalDate date
    Long clicks
    Long impressions

    static belongsTo = [campaign: Campaign, datasource: Datasource]

    static mapping = {
        version false
    }
}
