package com.adverity.warehouse

import com.adverity.warehouse.utility.DataLoader
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
class WarehouseApplication implements ApplicationRunner {
    private Boolean loadInitialData
    private DataLoader dataLoader

    WarehouseApplication(DataLoader dataLoader, @Value('${com.adverity.warehouse.loadInitialData:true}') Boolean loadInitialData) {
        this.dataLoader = dataLoader
        this.loadInitialData = loadInitialData
    }

    static void main(String[] args) {
        SpringApplication.run(WarehouseApplication, args)
    }

    @Override
    void run(ApplicationArguments args) {
        if (loadInitialData) {
            dataLoader.loadData("data.csv")
        }
    }
}
