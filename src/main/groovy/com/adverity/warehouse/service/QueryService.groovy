package com.adverity.warehouse.service

import com.adverity.warehouse.model.Metric
import com.adverity.warehouse.model.dto.Filter
import com.adverity.warehouse.model.type.Dimension
import com.adverity.warehouse.model.type.Aggregate
import com.adverity.warehouse.service.dto.QueryResult
import com.adverity.warehouse.utility.WarehouseConstants
import org.hibernate.transform.Transformers
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.math.RoundingMode
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
class QueryService {

    @Transactional(readOnly = true)
    List<QueryResult> query(
        Set<Aggregate> aggregates, Set<Dimension> groupBy, Set<Filter> filters,
        Integer pageSize = 25, Integer pageNumber = 1
    ) {
        List<QueryResult> queryResults = fetchResults(aggregates, groupBy, filters, pageSize, pageNumber)
        queryResults.each {
            if (aggregates.contains(Aggregate.CLICK_THROUGH_RATE)) {
                it.clickThroughRate = calculateClickThroughRate(it.clicks, it.impressions)
            }
        }

        queryResults
    }

    private List<QueryResult> fetchResults(
        Set<Aggregate> aggregates, Set<Dimension> groupBy, Set<Filter> filters,
        Integer pageSize = 25, Integer pageNumber = 1
    ) {
        Metric.createCriteria().list(max: pageSize, offset: (pageNumber -1) * pageSize) {
            resultTransformer(Transformers.aliasToBean(QueryResult))
            createAlias("datasource", "datasource")
            createAlias("campaign", "campaign")
            projections {
                aggregates.each {
                    switch (it) {
                        case Aggregate.CLICK:
                            sqlProjection('sum(clicks) as clicks', 'clicks', LONG)
                            break
                        case Aggregate.IMPRESSION:
                            sqlProjection('sum(impressions) as impressions', 'impressions', LONG)
                            break
                        case Aggregate.CLICK_THROUGH_RATE:
                            sqlProjection('sum(clicks) as clicks', 'clicks', LONG)
                            sqlProjection('sum(impressions) as impressions', 'impressions', LONG)
                            break
                    }
                }

                groupBy.each {
                    switch (it) {
                        case Dimension.DATASOURCE:
                            groupProperty("datasource.name", "datasource")
                            break
                        case Dimension.CAMPAIGN:
                            groupProperty("campaign.name", "campaign")
                            break
                        case Dimension.DATE:
                            groupProperty('date', 'date')
                            break
                    }
                }
            }

            filters.each {
                switch (it.property) {
                    case Dimension.DATASOURCE:
                        or { ->
                            it.values.each { String value ->
                                ilike("datasource.name", "${value}%")
                            }
                        }
                        break
                    case Dimension.CAMPAIGN:
                        or { ->
                            it.values.each { String value ->
                                ilike("campaign.name", "${value}%")
                            }
                        }
                        break
                    case Dimension.DATE:
                        if (it.values.dateFrom) {
                            ge("date", LocalDate.parse(it.values.dateFrom as String, DateTimeFormatter.ofPattern(WarehouseConstants.DEFAULT_DATE_FORMAT)))
                        }
                        if (it.values.dateTo) {
                            le("date", LocalDate.parse(it.values.dateTo as String, DateTimeFormatter.ofPattern(WarehouseConstants.DEFAULT_DATE_FORMAT)))
                        }
                        break
                }
            }
        } as List<QueryResult>
    }

    private BigDecimal calculateClickThroughRate(Long clicks, Long impressions) {
        (clicks && impressions) ? (clicks / impressions).setScale(5, RoundingMode.HALF_UP) : BigDecimal.ZERO
    }
}
