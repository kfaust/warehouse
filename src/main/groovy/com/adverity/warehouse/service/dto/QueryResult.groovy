package com.adverity.warehouse.service.dto

import com.fasterxml.jackson.annotation.JsonInclude
import groovy.transform.EqualsAndHashCode

import java.time.LocalDate

@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
class QueryResult {
    String datasource
    String campaign
    LocalDate date
    Long clicks
    Long Impressions
    BigDecimal clickThroughRate
}
