package com.adverity.warehouse.utility

import com.adverity.warehouse.model.Campaign
import com.adverity.warehouse.model.Datasource
import com.adverity.warehouse.model.Metric
import com.xlson.groovycsv.CsvParser
import groovy.util.logging.Slf4j
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.ResourceUtils

import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Slf4j
@Component
class DataLoader {

    @Transactional
    void loadData(String filePath) {
        log.info("Loading data into the database")

        File file = ResourceUtils.getFile("classpath:$filePath")
        Iterator csvContent = CsvParser.parseCsv(file.text)

        Map datasources = [:]
        Map campaigns  = [:]
        csvContent.each {
            if (!datasources.(it["Datasource"])) {
                datasources[it["Datasource"]] = new Datasource(name: it["Datasource"]).save()
            }
            if (!campaigns.(it["Campaign"])) {
                campaigns[it["Campaign"]] = new Campaign(name: it["Campaign"]).save()
            }

            new Metric(
                datasource: datasources[it["Datasource"]] as Datasource,
                campaign: campaigns[it["Campaign"]] as Campaign,
                date: LocalDate.parse(it["Daily"] as String, DateTimeFormatter.ofPattern(WarehouseConstants.DEFAULT_DATE_FORMAT)),
                clicks: it["Clicks"] as Long,
                impressions: it["Impressions"] as Long,
            ).save()
        }

        log.info("Data loaded successfully")
    }

}
