package com.adverity.warehouse.configuration

import org.h2.tools.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import java.sql.SQLException

@Configuration
class H2ServerConfiguration {
    @Value('${h2.tcp.port:9092}')
    String h2TcpPort;

    @Bean
    @ConditionalOnExpression('${h2.tcp.enabled:false}')
    Server h2TcpServer() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", h2TcpPort).start();
    }
}
