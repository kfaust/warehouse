package com.adverity.warehouse.request

abstract class PagedRequest {
    Integer pageSize = 25
    Integer pageNumber = 1
}
