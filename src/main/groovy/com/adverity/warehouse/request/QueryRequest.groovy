package com.adverity.warehouse.request

import com.adverity.warehouse.model.dto.Filter
import com.adverity.warehouse.model.type.Dimension
import com.adverity.warehouse.model.type.Aggregate

class QueryRequest extends PagedRequest {
    Set<Aggregate> aggregates = []
    Set<Dimension> groupBy = []
    Set<Filter> filters = []

    // In a real scenario there should be a much more robust request validation with validation error messages
    // possibly using a full fledged validation system like grails validation or JSR-303 Bean Validation for better Open API support
    Boolean validate() {
        !(aggregates.isEmpty() && groupBy.isEmpty())
    }
}
