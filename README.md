# Index

1. [Run](#run)

2. [Usage](#usage)

3. [Request Examples](#request-examples)

# Run

The application can be run locally with `./gradlew bootRun`. Tests can be run locally with `./gradlew test`.

The application is also deployed on a VPS with an IP address of `3.68.198.208`. This address will be used in the
examples below. If testing locally just replace it with `localhost:8080`.

# Usage
The given data is loaded at application startup.
The query API endpoint is located at `http://3.66.85.144:8080/query`. There are 5 different parameters in the request
body:

### aggregates

Array of property names which will be aggregated on data fetch. Valid values are `CLICK`, `IMPRESSION`
, `CLICK_THROUGH_RATE`. Example:

```json
{
  "aggregates": [
    "CLICK",
    "IMPRESSION"
  ]
}
```

### groupBy

Array of dimensions by which to group fetched data. Valid values are `DATASOURCE`, `CAMPAING`, `DATE`. Example:

```json
{
  "groupBy": [
    "DATASOURCE",
    "CAMPAIGN"
  ]
}
```

> At least one aggregate or a group by property must be given in the request body for performing a query.

### filters

Array of JSON objects for data filtering options. Each object consists of a `property` value and an `values` array.
Valid `property` values to filter by are `DATASOURCE`, `CAMPAIGN`, `DATE`.

#### DATASOURCE and CAMPAIGN filtering

For `DATASOURCE` and `CAMPAIGN` filters the `values` array can contain any number of strings with which the data needs
to be filtered by. Partial string matching from the left side will be taken in consideration, so for example you can
query results for a datasource named `test` with only giving `t` as a value. Example:

```json
{
  "filters": [
    {
      "property": "DATASOURCE",
      "values": [
        "Goo",
        "Facebook"
      ]
    },
    {
      "property": "CAMPAIGN",
      "values": [
        "Adventmarkt"
      ]
    }
  ]
}
```

#### DATE filtering
Date filtering can be achieved by giving the `DATE` value to the `property` parameter alongside the date range in the `values` parameter.
The date range is a json object, and it consists of two additional values `dateFrom` and `dateTo`. Only one of those values is mandatory when using date filtering.
The dates value must be in the form of `MM/dd/yy`. Example:
```json
{
  "filters": [
    {
      "property": "DATE",
      "values": {
        "dateFrom": "01/01/19",
        "dateTo": "10/10/20"
      }
    }
  ]
}
```
or
```json
{
  "filters": [
    {
      "property": "DATE",
      "values": {
        "dateFrom": "01/01/19"
      }
    }
  ]
}
```

### Paging
The query request supports data pagination. Default paging size is 25. Pagination can be adjusted with `pageSize` and `pageNumber` properties. Example:
```json
{
  "pageSize": 50,
  "pageNumber": 2
}
```

# Request Examples
The application is integrated with `swagger-ui` via `Open API`. It is not fully documented but rather it serves as a convenience to test the endpoint without any third party REST clients.
The swagger web interface can be accessed at [http://3.66.85.144:8080/swagger-ui.html](http://3.66.85.144:8080/swagger-ui.html).

## Task Query Examples

### Total Clicks for a given Datasource for a given Date range
```http request
POST http://3.66.85.144:8080/query

{
  "aggregates": [
    "CLICK"
  ],
  "groupBy": [
    "DATASOURCE"
  ],
  "filters": [
    {
      "property": "DATASOURCE",
      "values": [
        "Google"
      ]
    },
    {
      "property": "DATE",
      "values": {
        "dateFrom": "01/01/19",
        "dateTo": "01/10/19"
      }
    }
  ]
}
```

### Click-Through Rate (CTR) per Datasource and Campaign
```http request
POST http://3.66.85.144:8080/query

{
  "aggregates": [
    "CLICK_THROUGH_RATE"
  ],
  "groupBy": [
    "DATASOURCE",
    "CAMPAIGN"
  ]
}
```

### Impressions over time (daily)
```http request
POST http://3.66.85.144:8080/query

{
  "aggregates": [
    "IMPRESSION"
  ],
  "groupBy": [
    "DATE"
  ],
  "pageSize": 100,
  "pageNumber": 1
}
```
